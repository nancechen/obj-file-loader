/**
 * Created by Nance on 21/9/2017.
 */

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;

public class Demo implements GLEventListener {

    private GLU glu = new GLU();
    private float rtri = 0.0f;
    public ObjLoader v;

    @Override
    public void display(GLAutoDrawable drawable) {
        final GL2 gl = drawable.getGL().getGL2();

        // Clear The Screen And The Depth Buffer
        gl.glClear( GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT );
        gl.glLoadIdentity(); // Reset The View
        gl.glTranslatef(-0.5f, 0.0f, -6.0f); // Move the triangle
        gl.glRotatef(rtri, 0.0f, 1.0f, 0.0f);
        gl.glBegin(GL2.GL_TRIANGLES);
        gl.glEnable(GL2.GL_TEXTURE);


        for (Face face : v.faces) {
            Vector3f n1 = v.normals.get((int) face.normal.x -1);
            Vector2f t1 = v.textures.get((int) face.texture.x -1);
            Vector3f v1 = v.vertices.get((int) face.vertex.x -1);

            Vector3f n2 = v.normals.get((int) face.normal.y -1);
            Vector2f t2 = v.textures.get((int) face.texture.y -1);
            Vector3f v2 = v.vertices.get((int) face.vertex.y -1);

            Vector3f n3 = v.normals.get((int) face.normal.z -1);
            Vector2f t3 = v.textures.get((int) face.texture.z -1);
            Vector3f v3 = v.vertices.get((int) face.vertex.z -1);

            gl.glVertex3f(v1.x, v1.y, v1.z);
            gl.glTexCoord2f(t1.x, t1.y);
            gl.glNormal3f(n1.x, n1.y, n1.z);

            gl.glVertex3f(v2.x, v2.y, v2.z);
            gl.glTexCoord2f(t2.x, t2.y);
            gl.glNormal3f(n2.x, n2.y, n2.z);

            gl.glVertex3f(v3.x, v3.y, v3.z);
            gl.glTexCoord2f(t3.x, t3.y);
            gl.glNormal3f(n3.x, n3.y, n3.z);


        }
        gl.glDisable(GL2.GL_TEXTURE);

        gl.glEnd();
        gl.glFlush();
        rtri += 0.2f;
    }

    @Override
    public void dispose( GLAutoDrawable drawable ) {
    }

    @Override
    public void init( GLAutoDrawable drawable ) {
        v = new ObjLoader();
        v.LoadFile("test");
    }

    @Override
    public void reshape( GLAutoDrawable drawable, int x, int y, int width, int height ) {

        final GL2 gl = drawable.getGL().getGL2();
        if(height <=0)
        height = 1;

        final float h = ( float ) width / ( float ) height;
        gl.glViewport( 0, 0, width, height );
        gl.glMatrixMode( GL2.GL_PROJECTION );
        gl.glLoadIdentity();

        glu.gluPerspective( 45.0f, h, 1.0, 20.0 );
        gl.glMatrixMode( GL2.GL_MODELVIEW );
        gl.glLoadIdentity();
    }

    public static void main( String[] args ) {

        final GLProfile profile = GLProfile.get( GLProfile.GL2 );
        GLCapabilities capabilities = new GLCapabilities( profile );

        // The canvas
        final GLCanvas glcanvas = new GLCanvas( capabilities );
        Demo triangle = new Demo();

        glcanvas.addGLEventListener( triangle );
        glcanvas.setSize( 400, 400 );

        final JFrame frame = new JFrame ( "Demo" );

        frame.getContentPane().add( glcanvas );
        frame.setSize( frame.getContentPane().getPreferredSize() );
        frame.setVisible( true );

        final FPSAnimator animator = new FPSAnimator(glcanvas,300,true);
        animator.start();
    }

}