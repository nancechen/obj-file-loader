/**
 * Created by Nance on 19/9/2017.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ObjLoader{
    public List<Vector3f> vertices;
    public List<Vector2f> textures;
    public List<Vector3f> normals;
    public List<Face> faces;

    public void LoadFile(String filename){
        FileReader fr = null;
        try {
            File fl = new File("/Users/Nance/Downloads/" + filename + ".obj");
            fr = new FileReader(fl);


        } catch (FileNotFoundException e) {
            System.err.println("Could not find file.");
            e.printStackTrace();
        }


        BufferedReader reader = new BufferedReader(fr);
        String line;
        vertices = new ArrayList<Vector3f>();
        textures = new ArrayList<Vector2f>();
        normals = new ArrayList<Vector3f>();
        faces = new ArrayList<Face>();

        //reading file 
        try {

            while ((line = reader.readLine()) != null) {
                String[] currentLine = line.split(" ");
                if (line.startsWith("#")) {
                    continue;
                } else if (line.startsWith("v ")) {
                    Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
                    vertices.add(vertex);
                } else if (line.startsWith("vt ")) {
                    Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]));
                    textures.add(texture);

                } else if (line.startsWith("vn ")) {
                    Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
                    normals.add(normal);

                } else if (line.startsWith("f ")) {

                    Vector3f vertexIndices = new Vector3f(
                            Float.valueOf(line.split(" ")[1].split("/")[0]),
                            Float.valueOf(line.split(" ")[2].split("/")[0]),
                            Float.valueOf(line.split(" ")[3].split("/")[0]));
                    Vector3f textureIndices = new Vector3f(Float.valueOf(line.split(" ")[1].split("/")[1]),
                            Float.valueOf(line.split(" ")[2].split("/")[1]),
                            Float.valueOf(line.split(" ")[3].split("/")[1]));

                    Vector3f normalIndices = new Vector3f(Float.valueOf(line.split(" ")[1].split("/")[2]),
                            Float.valueOf(line.split(" ")[2].split("/")[2]),
                            Float.valueOf(line.split(" ")[3].split("/")[2]));


                    faces.add(new Face(vertexIndices, textureIndices, normalIndices));
                }

            }
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

}
